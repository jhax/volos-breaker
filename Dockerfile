##################################
# STEP 1 build executable binary #
##################################
FROM golang@sha256:0991060a1447cf648bab7f6bb60335d1243930e38420bee8fec3db1267b84cfa as builder
# FROM golang as builder
# FROM golang:latest

# Install git + SSL ca certificates
# Git is required for fetching the dependencies
# Ca-certificates is required to call HTTPS endpoints
RUN apk update && apk add --no-cache git ca-certificates && update-ca-certificates

# Create appuser
ENV USER=appuser
ENV UID=10001

# See https://stackoverflow.com/a/55757473/12429735
RUN mkdir -p $GOPATH/src/gitlab.com/jhax/volos-breaker/
RUN adduser \    
    --disabled-password \    
    --gecos "" \    
    --home "/nonexistent" \    
    --shell "/sbin/nologin" \    
    --no-create-home \    
    --uid "${UID}" \    
    "${USER}"WORKDIR $GOPATH/src/gitlab.com/jhax/volos-breaker/
WORKDIR $GOPATH/src/gitlab.com/jhax/volos-breaker/
COPY . .

# Fetch dependencies.
# Using go mod with go 1.11
# RUN go mod download
# RUN go mod verify
RUN pwd
RUN ls -l
RUN echo "####\n####\n##\nGO MOD FILE: $GOPATH/go.mod\n##\n####\n####"
RUN go get .

# Build the binary
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/hello

##############################
# STEP 2 build a small image #
##############################

# Import from builder
FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Copy our static executable
COPY --from=builder /go/bin/hello /go/bin/hello

# Use an unprivileged user.
USER appuser:appuser

# Run the hello binary.
ENTRYPOINT ["/go/bin/vAPI"]