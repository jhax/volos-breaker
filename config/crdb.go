package config

import (
	"fmt"

	"gitlab.com/jhax/volos-breaker/entities"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Database *gorm.DB
var crdbUser string = "test"
var crdbPass string = "testpass"
var crdbDB string = "products"
var crdbHost string = "192.168.64.4"
var crdbPort string = "30903"
var crdbSSL string = "allow"
var crdbURI = fmt.Sprintf("postgresql://%s:%s@%s:%s/%s?sslmode=%s",
	crdbUser,
	crdbPass,
	crdbHost,
	crdbPort,
	crdbDB,
	crdbSSL)

func Connect() error {
	fmt.Println("connection string: ", crdbURI)
	var err error

	Database, err = gorm.Open(postgres.Open(crdbURI),
		&gorm.Config{})

	if err != nil {
		panic(err)
	}

	Database.AutoMigrate(&entities.Product{})

	return nil
}
