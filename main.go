package main

import (
	"fmt"
	"image"
	"log"

	_ "image/jpeg"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/jhax/volos-breaker/config"
	"gitlab.com/jhax/volos-breaker/handlers"
)

func main() {
	fmt.Println("Volos API Starting...")

	fmt.Println("Loading Image")

	var myImg image.Image
	myImg = handlers.LoadImage("assets/img/ps5_console.jpg")
	_ = myImg

	// fmt.Println("Printing Image Now:\n", myImg)

	app := fiber.New()

	config.Connect()

	app.Get("/products", handlers.GetProducts)
	app.Get("/products/:id", handlers.GetProduct)
	app.Post("/products", handlers.AddProduct)
	app.Put("/products/:id", handlers.UpdateProduct)
	app.Delete("/products/:id", handlers.RemoveProduct)

	log.Fatal(app.Listen(":20888"))
}
