package entities

import (
	"time"

	"gorm.io/gorm"
)

type Product struct {
	gorm.Model
	Name          string    `json:"name"`
	Store         string    `json:"store"`
	Sku           string    `json:"sku"`
	RetailPrice   string    `json:"retail_price"`
	PurchasePrice string    `json:"purchase_price"`
	ProductImage  string	`json:"product_image"`
	IsTracked     bool      `json:"isTracked" gorm:"default:true"`
	CreatedAt     time.Time // Set to current time if it is zero on creating
	UpdatedAt     time.Time
}
